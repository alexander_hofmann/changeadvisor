This work set out with the goal of rewriting the original Proof-of-Concept, going beyond the PoC stage, as a full-fledged application, with the specific goal to improve aspects such as performance, maintainability, extensibility, and usability.

This work, however, is not a research thesis. As such a \textit{quantitative} review of the results, is not possible. There are no experimental values, and no hypothesis to be confirmed or confuted. Rather, we do a \textit{qualitative} evaluation, by reviewing the aspect mentioned above, the goals set out, and by comparison with the PoC.

It is important to remember however, that the difference between \texttt{ChangeAdvisor} and the PoC is considerable. The previous iteration of \texttt{ChangeAdvisor} was developed as a tech demo and as a throwaway PoC and because of this, aspects such as maintainability and extensibility were, at the time, a non-issue.


We review \texttt{ChangeAdvisor}, under the following aspects:
\begin{itemize}
\item Maintainability: ease, for a developer, to apply fixes in a timely manner; brittleness of the system.
\item Extensibility: openness to extensions, with or without code changes.
\item Performance: speed of execution and memory consumption.
\item Usability: ease of use for the end-user, flexibility for future research.
\end{itemize}

\section{Maintainability}
Refactoring and bug fixing is a natural process of each software project. Even with good requirements and top developers, code has to be flexible enough to be modified: bugs will always find a way to slither in, code has to be changed to reflect the product owner's vision and preference, new tools, libraries, or techniques might be introduced for many different reasons.
Over the course of this project, bugs were, naturally, introduced, and requirements slightly changed and thus a certain deal of refactoring and bug fixing was needed.

As we have seen in Section~\ref{changeAdvisorMain}, the core of the business logic is implemented through the use of \textbf{Spring Batch}. \textbf{Spring Batch} makes it easier to implement processes, defined as a list of steps, promoting \textit{single responsibility principle} and \textit{loose coupling}. As such, many of the main components in the \textit{pipelines}, work completely independently from one another, and can also be tested in complete isolation. This, simple, fact greatly helps, when modifying code. 

A concrete example of this, was when \textbf{TFIDF} was added to \texttt{ChangeAdvisor}, the entities used as input for the \textit{linker} were different, depending on the clustering algorithm used. Through a simple generalization in the linker interface (\texttt{LinkableReview}, see \ref{sec:changeadvisor-linker}), the processor only had to modify its input parameter to that of the generalization. Then, we added a new \texttt{ItemReader} for the new input type, which could, thanks to \textbf{Spring Batch}, simply be plugged in the definition of the job.

A small test to the maintainability, will be the fine-tuning process that \texttt{ChangeAdvisor} will undergo in future. While all processes were implemented, mostly as per the PoC, it became apparent during development, that some processes could be streamlined. Some steps in the preprocessing stage for example might be omitted. The linker step also does some string manipulation, which might not be needed, considering prior preprocessing.

Over the course of this project, we strived to always apply software engineering principles and best practices, in order to improve the maintainability of the system. 
This is reflected in the following examples:

\begin{itemize}
\item Interfaces and composition, when possible, following the  \textit{strategy pattern}.
\item Inversion of Control (IOC), through Spring's dependency injection
\item Separation of concerns, e.g. \texttt{Item\{Reader, Processor, Writer\}}, decoupled client and server.
\item Code re-use, e.g. review and source code preprocessing, use of well-established libraries and frameworks.
\end{itemize}

Thus, under the maintainability aspect, the current version of \texttt{ChangeAdvisor}, is more advanced than the original PoC.

\section{Extensibility}
Extensibility refers to the openness of a system to extension, both with code changes and without. It is the degree to which a system can be changed, while also considering the amount of effort required for such change to happen. Examples of this are: the addition of a new feature, or the introduction of a new UI in the form of a mobile app.

The PoC was non-extensible, as it did not need to be. This work now exists to apply the original approach in a new framework, allowing it to become extensible, as we have seen above.
Thus, we identify the two main assets providing extensibility to \texttt{ChangeAdvisor}, at the macroscopic level:
\begin{itemize}
\item Batch approach
\item Decoupled front- and back-end
\end{itemize}


\paragraph{Extensibility through Batch Approach}
Under this aspect, \texttt{ChangeAdvisor} should be primed for success, indeed the designed batch approach, makes the existing jobs (\textit{pipelines)} flexible enough to swap out components.
It allows for the design of entirely new jobs, while leaving the existing pipelines unaltered, since they are independent of one another. Considering, also, the classic layered architecture of the back-end server, implemented in \texttt{ChangeAdvisor}, it makes it particularly easy to define new processes, and connect these processes to the API for triggering and then consumption of the results.

\paragraph{Decoupled front- and back-end}
The provided REST API layer, allows for unlimited extensibility in terms of clients. Indeed, the ubiquity of HTTP clients, means that it is possible to extend \texttt{ChangeAdvisor} with new UIs and clients, for virtually any kind of system. Possibilities for the future include, new web clients, mobile apps, and plug-ins for IDEs. It would be interesting, to integrate \texttt{ChangeAdvisor} into existing CI/CD pipelines. For example, with every new commit to the master branch that triggers a redeploy of the application, the source code is also analyzed, and the new results are integrated with the reviews imported between this analysis and the previous one. This way, we could run the process against the version of the code that is actually in use by end-users.

\section{Performance}
Performance comparisons between the PoC and the current iteration of \texttt{ChangeAdvisor} is an interesting aspect, as it is not as clear-cut as with the other facets. It rather is much more of a trade-off between memory consumption and speed of execution.
By having a back-end and front-end server running, we consume overall considerably more memory than the PoC, since once the PoC is done, it can exit, freeing all of its memory. In the case of servers, we constantly have a set of threads hogging the memory, since it has certain availability requirements, that a command line interface (CLI) process does not have. On the other hand, overall speed of execution is dramatically increased. Often during development of the core \texttt{ChangeAdvisor} process, we compared the results and execution times of the PoC versus those of the newer implementation and found that each step of the process, is quicker.
An exception is made for the source code and reviews import. With the PoC these are responsibility of the user. So it is not possible to compare the performance of these steps with the new version.

\paragraph{String manipulation.}
The biggest difference, in terms of execution speed, is in those steps, which are particularly heavy in string manipulation. This includes the preprocessing steps, and the \texttt{ChangeAdvisorLinker}.
Especially, the performance of the preprocessing stage, is orders of magnitude better. One of the main reasons we have identified is that the PoC heavily relied on string concatenation using the "+" operator. Duplicate removal, as an example, works as follows: (i) input to the method is passed as a single string, (ii) which is then split using the \textit{split()} method, (iii) to create the result, we join the tokens back together using the "+" operator, but first we check, using the \textit{contains()} method, whether the token is already contained in the result string. The performance overhead caused by this approach is significant. The are two problems with this approach:
\begin{itemize}
\item  string concatenation using the "+" operator, means that, for each concatenation, we must first create a character buffer large enough to contain both tokens, in to which, we then copy each character. Doing this operation for every token of every document in a corpus, becomes costly, when the size of the corpus is in the order of thousands. In fact, a simple computation shows, that its complexity is $O(n^2)$, in other words, it has a quadratic running time \footnote{\url{http://www.pellegrino.link/2015/08/22/string-concatenation-with-java-8.html}}. In some cases the compiler can optimize this process by using a \texttt{StringBuilder} instance. However, in the case of dynamic data, i.e. data not know at compile-time, it cannot do this optimization. Explicitly using a \texttt{StringBuilder} brings the running time down to $O(1)$ amortized.

\item use of the \textit{contains()} method on a string, means we need to iterate over each character of the increasing result string, to match the token we are searching for. This results in $O(n)$ complexity. By using a \texttt{Set} for the removal of duplicates, we get $O(1)$ running time, since the \textit{contains()} is optimized for this operation. In fact, it computes a hash of the value we are searching for, and uses that as the index of the backing collection, in order to find the searched token.
\end{itemize}

Some manual testing showed, that for the linking step, which also does extensive string manipulation, the run-time of the PoC was around 60 minutes, while in the new version, it was about 1 minute, while achieving similar results (small differences in how the tokens are manipulated lead to a variation in the results).

\paragraph{Clustering.}
The PoC implemented the \textbf{HDP}~\cite{teh2005sharing} in Python. For this work, we ported the Python code in Java. Thus, the only difference in terms of speed is given by the speed of the Python interpreter present on the system, versus that of the Java compiler and JVM running on the same system. On the machines tested, the execution time of the Java version was about 10 times quicker. However, no actual benchmarking was done, and performance, especially for Python, greatly depends on the interpreter used, thus these values are to be taken with a grain of salt. In any case, compiled languages, do tend to have an advantage, in terms of speed, over interpreted languages. It is important to note, that these values come from observations made while developing the newer iteration, the goal then was never to measure speed of execution, but rather to check whether the results were comparable.

\section{Usability}
Usability refers to the ease of use of a tool, i.e. how easy it is to use and to learn. Usability is a make-it or break-it criterion: no one wants to be stuck with a tool that is annoying to use, and is one of the main reasons for the complete rewrite of \texttt{ChangeAdvisor}. The previous iteration of the tool, was a \textit{CLI} software. Considering the target audience of \texttt{ChangeAdvisor}, it is not a matter of learning, but rather continuous use: indeed, even though, most developers should be comfortable enough with a shell, it does not mean that they might want to spend any more time than necessary in it. Additionally, a CLI precludes the possibility for the visual exploration of results. The field of data visualization has already shown the importance of visually analyzing data, in order to make even extremely large data sets digestible, which in turn leads to better decision making. Another disadvantage of the PoC, in terms of usability, is the way the reviews are fed to it. In order to start the process, we must create a flat file containing all user feedback. Which means that the developer needs to, by some other means, export their reviews and then create this file. This process has to be done periodically, as new reviews come in.
This, greatly, hurts the usability of the PoC, since this process, ends up involving multiple tools, just to start \texttt{ChangeAdvisor}.

The new version of \texttt{ChangeAdvisor} should represent a major step forward. Indeed, the possibility to automatically import the reviews from the \textit{Google Play Store} on a schedule and the source code from \textit{VCS}, significantly lowers the effort needed to use the tool.

Additionally, this newer version, thanks to its graphical interface, allows for the possibility to better analyze the results of \texttt{ChangeAdvisor}. This is not only an advancements, in terms of ease of use, but also of overall pleasure of using the tool.

Future works, might implement historical analysis of feedback, thanks to the database backing \texttt{ChangeAdvisor}, which would enable for even more interactivity and functionality with the system, increasing the usability of the tool.
