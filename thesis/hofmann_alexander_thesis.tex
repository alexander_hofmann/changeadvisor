\documentclass{seal_thesis}
\thesisType{Bachelor Thesis}
\date{\today}
\title{ChangeAdvisor}
\subtitle{A tool for Recommending and Localizing Change Requests for Mobile Apps based on User Reviews}
\author{Alexander Hofmann}
\home{Zurich}
\country{Switzerland}
\legi{11-916-863}
\prof{Prof. Dr. Harald C. Gall}
\assistent{Dr. Sebastiano Panichella}
\email{alexander.hofmann@uzh.ch}
\url{https://bitbucket.org/alexander\_hofmann/changeadvisor}
\begindate{10.07.2017}
\enddate{10.01.2018}

\usepackage{color}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{varwidth}
\usepackage[normalem]{ulem}
\usepackage[bottom]{footmisc}

\useunder{\uline}{\ul}{}

\begin{document}
\maketitle

\frontmatter

\begin{acknowledgements}
The success and final outcome of this project, its implementation, and this thesis, has been one of the most rewarding and challenging experiences of my life. It would not have been possible without the help of a few people. In the following I would like to express my gratitude to those that have supported me, in many different ways, in order to reach the conclusion of the Bachelor thesis. First, I wish to present my special thanks to all those remarkable people who have contributed directly to the realization of my thesis. After that, I wish to express my appreciation and gratitude, to those who indirectly allowed me to reach such an important milestone in my life: the Bachelor's degree.

First and foremost, I owe a debt of gratitude to my advisor, Dr. Sebastiano Panichella, for offering me this interesting and challenging assignment, for his enthusiasm and trust placed in me, giving me the freedom to tackle this project as I saw fit. The door to his office was always open, whenever I needed his help or guidance.  He consistently allowed this paper to be my own work, but steered me in the right direction whenever I needed it. I could not have imaged having a better advisor and mentor for my Bachelor's project.

Besides my advisor, I am sincerely grateful to Prof. Dr. Harald C. Gall for giving me the opportunity to work together with the Software Evolution \& Architecture Lab at the University of Zurich.

Finally, I must spend a few words to express my very profound gratitude to those people, who provided me with unfailing support and continuous encouragement throughout my entire academic career. To my wonderful girlfriend, Tramy, whose love and support has helped me more than she will ever realize. And to my mother, Elizabeth, for always believing in me and supporting me in my endeavor, even through the difficulties and setbacks, since the beginning of my career.

This accomplishment would not have been possible without all of them.
\end{acknowledgements}

\begin{abstract}
User feedback plays a paramount role in the development and maintenance of mobile applications. The experience an end-user has with an app, is a key concern when creating and maintaining a successful product. Consequently, developer teams need to incorporate opinions and feedback of end-users in the evolutionary process of their software, in order to meet market requirements.
However, existing app distribution platforms provide limited support for developers to systematically filter, aggregate, and classify user feedback to derive requirements. 
Moreover, manually reading each user review to gather useful feedback is not feasible, considering the sheer amount of reviews popular apps have received and continue to receive day after day. Even then, the gathered information is restricted to user reviews, and no systematic way exists to link user feedback to the related source code components to be changed, a task that requires an enormous manual effort and is highly error-prone.

To fill this void, Palomba~{\em et al.}~\cite{Palomba2017} introduced \textit{ChangeAdvisor}, an approach able to cluster user reviews, useful for software maintenance tasks, into topics, in order to recommend developers, which source code entities to change. 
This already greatly simplifies the work for the developer, as it is not necessary anymore to sift through the reviews, divide them in valuable or valueless feedback, then try to figure out, which source code component is affected from the proposed changes.
However \textit{ChangeAdvisor}, until now, existed only as a \textit{Proof of Concept}, which was limited in terms of extensibility and maintainability, as well as in functionality.

Thus, this thesis implements \textit{ChangeAdvisor} as a library, in order to support future extensions of the approach, as well as a client-server application, to allow developers to fully leverage the power of the information contained in user feedback.
\end{abstract}


\begin{zusammenfassung}
Das Benutzerfeedback spielt eine wichtige Rolle bei der Entwicklung und Wartung von Mobile Apps.
Die Erfahrung, die ein Endbenutzer mit einer App hat, ist einer der wichtigsten Punkte bei der Entwicklung und Wartung eines erfolgreichen Produkts.
Aus diesem Grund müssen Entwicklerteams Meinungen und Feedbacks von End-Usern in den Entwicklungsprozess ihrer Software einfliessen lassen, um den Marktanforderungen gerecht zu werden. 
Bestehende App Stores bieten Entwicklern jedoch nur begrenzte Unterstützung, um Benutzerfeedbacks systematisch zu filtern, zu aggregieren, und zu klassifizieren.
Ebenso fehlt es an Techniken, um aus diesen Feedbacks Anforderungen an das Produkt herzuleiten.
Ausserdem ist das Lesen jener Feedbacks nicht praktikabel, wenn man die Menge an täglichen Bewertungen für beliebte Apps berücksichtigt.
Selbst wenn die Menge an Bewertungen kein Problem wäre, sind die gesammelten Informationen auf Benutzerbewertungen beschränkt und es gibt keine Möglichkeit die Benutzerfeedbacks mit den 
zu ändernden Quellcode-Komponenten systematisch zu verlinken. 
Dies wäre nur mit enormen manuellem Aufwand zu ermöglichen und wäre sehr fehleranfällig.

Um diese Lücke zu füllen, führte Palomba~{\em et al.}~\cite{Palomba2017} \textit{ChangeAdvisor} ein.
Der \textit{ChangeAdvisor} Ansatz erlaubt es, Benutzerfeedbacks, welche für die Softwarewartung nützlich sind, nach Themen zu gruppieren. 
Dank diesen Themengruppen sehen die Entwickler welche Quellcode-Komponenten verbessert werden sollten.
Dadurch wird die Arbeit der Entwickler bereits vereinfacht, da es nicht mehr notwendig ist die Reviews manuell zu durchsuchen, zu unterteilen und den Quellcode-Komponenten zuzuordnen.

Bisher war \textit{ChangeAdvisor} nur ein \textit{Proof of Concept}, welcher in Bezug auf Wartbarkeit, Erweiterbarkeit und Funktionalität sehr eingeschränkt war.
In dieser Bachelorarbeit wird \textit{ChangeAdvisor} deshalb als Softwarebibliothek implementiert, um zukünftige Erweiterungen zu unterstützen.
Zusätzlich wurde eine Client-Server App implementiert, welche es Entwicklern erlaubt die gesamten Informationen aus Benutzerfeedbacks zu nutzen.

\end{zusammenfassung}

\tableofcontents
\listoffigures
\listoftables
\lstlistoflistings

\mainmatter
\chapter{Introduction}
\input{introduction}

\lstset{
frame=l,
xleftmargin={1.25cm},
numbers=left,
stepnumber=1,
firstnumber=1,
numberfirstline=true
}


\chapter{Related Work}
\label{related}
\input{related}

\chapter{Approach}
\label{approach}
\input{approach}

\chapter{ChangeAdvisor - the Tool}
\label{changeAdvisorMain}
\input{implementation}

\chapter{Evaluation}
\label{evaluation}
\input{results}

\chapter{Conclusions and Future Work}
\label{sec:conclusions}
\input{conclusions}

\backmatter
\bibliographystyle{alpha}
\bibliography{biblio}

\end{document}
