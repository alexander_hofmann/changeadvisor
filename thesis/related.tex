In this section, I will briefly summarize the research done in the context of \textit{App Store Review Mining} and \textit{Linking} of information contained in free-form text to source code.

\section{App Store Mining for Software Engineering}
User reviews provide an invaluable source of data regarding user perception of a product.
Indeed, Mudambi and Schuff showed in 2010, that there is a strong correlation between user reviews and purchasing decisions of products on amazon.com \cite{Mudambi:2010:MHO:2017447.2017457}. The advent of App Stores, such as \textit{Google Play Store} and Apple's \textit{App Store} in 2008, made it easier than ever to access software and review apps. This is made evident by the download and review numbers, presented in Table~\ref{app-stats-table}, of popular applications. We can see that in the brief span of seven years, an app like \textit{Facebook} had between 1 and 5 billions unique installs, considering \textit{Android} alone, and over 72 million users commented on their problems, their praise, and their wishes for new features.

\begin{table}[]
\centering
\label{app-stats-table}
\caption[Release year, number of unique installs and reviews for popular apps on the \textit{Google Play Store}]{Release year, number of unique installs and reviews for popular apps on the \textit{Google Play Store} (standings Nov. 2017\footnotemark).}
\label{my-label}
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{App name}           & \textbf{Release Year} & \textbf{Unique Installs (in millions)} &\textbf{ Reviews (in millions)} \\ \hline\hline
Facebook           & 2010         & 1'000 - 5'000 & 72 \\ \hline
Facebook Messenger & 2011         & 1'000 - 5'000 & 49 \\ \hline
Whatsapp          & 2010         & 1'000 - 5'000 & 60 \\ \hline
Pokemon GO         & 2016         & 100 - 500 & 9 \\ \hline
\end{tabular}
\end{table}

\footnotetext{Google Play Store statistics, as of November 2017:\\\url{https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en}\\\url{https://play.google.com/store/apps/details?id=com.facebook.orca}\\\url{https://play.google.com/store/apps/details?id=com.whatsapp}
\\\url{https://play.google.com/store/apps/details?id=com.nianticlabs.pokemongo}}




It is no wonder then, that researchers started to come up with ways to capitalize on this wealth of, albeit unstructured, information to extract useful structured data.
This brings us to Harman~\textit{et~al.}~\cite{Harman:MSR:2012} which introduced the concept of \textit{App Store Mining} as a form of \textit{Mining Software Repositories}.
In their work, they showed a strong correlation between an app's rating and download rank.
Their approach can roughly be described in the following three steps: (i) extract raw data from the App store, (ii) parse data extracting all attributes regarding price, ratings, and description, and (iii) use data mining to extract relevant features from gathered description.

The result of which are statistics regarding technical, customer, and business aspects of each app.

Many researchers focused on the analysis of user reviews, as can be seen in the survey by Martin~\textit{et~al.}~\cite{Martin:tse2017}. In their work, the authors summarized and identified the following seven subfields of 	\textit{App Store Analysis}. 

\begin{itemize}
\item API Analysis: studies that extract feature information by analyzing app APKs and/or source code together with non-technical data~\cite{Martin:tse2017}.

\item Feature Analysis: studies that extract features from sources, other than source code and requirements.
In this subfield, information is extracted by analyzing descriptions, API usage, manifest files, decompiled source strings, categories, and permissions~\cite{Martin:tse2017}.

\item Release Engineering: studies that extract feature information from app releases. It analyzes how the content changes between releases, the effects on user perception, and the releases strategies adopted by developers (e.g. app category diversification, free vs. paid, update rollouts)~\cite{Martin:tse2017}.

\item Review Analysis: studies that extract feature information by examining user feedback~\cite{Martin:tse2017}.

\item Security: studies that explore various security aspects of apps, such as faults in code leading to vulnerabilities, malicious apps, permissions, plagiarism, privacy, and update behaviour of users~\cite{Martin:tse2017}.

\item Store Ecosystem: studies that explore "the ecosystem of each app store and the differences between them"~\cite{Martin:tse2017}.

\item Size and Effort Prediction: studies that predict size or effort based on the list of functionalities of an app~\cite{Martin:tse2017}.
\end{itemize}


Of particular interest for this work are: (i) \textit{API Analysis}, and (ii) \textit{Review Analysis}.

\subsection{API~Analysis} API analysis refers to techniques that attempt to extract feature information by examining open-sourced source code and combine such features with non-technical data (e.g. ratings). 
Martin~\textit{et~al.}~\cite{Martin:tse2017} identified 4 subfields: (i) \textit{API usage}, (ii) \textit{Class Reuse and Inheritance}, (iii) Faults, (iv) \textit{Permissions and Security}.

\paragraph{API Usage.}
Azad~\cite{library979917} mined apps from Google Play Store and F-droid, in particular, method calls to suggest (i) related calls from the VCS history, (ii) similar calls found in Stack-Overflow posts, (iii) possible copyright and license violations with open-source software.
Tian~\textit{et~al.}~\cite{Tian:2015:ICSE} proposed a case study encompassing over 1400 apps, in which they identified 28 possible factors that might determine the rating of apps. Through this study they determined that, in high-rated apps, 17 of the 28 characteristics proposed, deviate significantly from low-rated apps. The most influential factors they found were: size of an app, use of promotional images, and target SDK~\cite{Tian:2015:ICSE}.

\paragraph{Faults.}
Many studies have concluded that there is a link between Fault and change-prone APIs and low rated apps~\cite{Martin:tse2017}. Indeed, Linares-Vasquez~\textit{et~al.}~\cite{bavota2015impact} studied how the use of change- and fault-prone APIs impacted the success of apps. Their results indicate that there is an inverse relation between the usage of such APIs and their rating.

Syer~\textit{et~al.}~\cite{syer2015studying} proposed to use the degree of platform dependence (or independence) as an indicator of software quality. The authors argue that system specific APIs are rapidly evolving, and thus often introduce defects. Through their study they found, that the more defect-prone source files are those that depend more heavily on the platform, thus degree of platform dependence could be used to prioritize QA resources.

Khalid~\textit{et~al.}~\cite{khalid2016examining} proposed the use of popular static analysis tool \texttt{FindBugs} to find possible relations between warnings and user rating. The authors found that the categories "bad practice", "internationalization", and "performance", appeared more frequently in low rated apps, and that seemed to reflect user perception.

\subsection{Review~Analysis} Review analysis refers instead to techniques that attempt to extract features by analyzing user reviews, by employing \textit{Natural Language Processing}, \textit{Sentiment Analysis}, and \textit{Topic Modelling}. The goals of this analysis are multiple, the most important of which are mentioned here:
\begin{itemize}
\item Review classification
\item Determining factors that affect user feedback
\item Extraction of bug reports and feature requests
\item Review prioritization
\item Review summarisation
\end{itemize}

These subfields are of particular interest for this work, since they are the foundation it is based on: by taking the work done in both categories and merging it into a tool capable of correlating \textit{reviews} and \textit{code}, as seen in Chapter~\ref{approach}.

In the context of \textit{App Store Analysis}, Pagano~\textit{et~al.}~\cite{Pagano:2013:UIS:2486788.2486920} showed the need for automated tools for review analysis. Indeed they proposed a case study which showed, that user feedback contains important information for developers, which helps in software maintenance tasks. However manually analyzing reviews, especially for popular apps, requires a considerable time investment. This is caused by the fact that user feedback is unstructed, and that the quality and content varies greatly between users~ \cite{Pagano:2013:UIS:2486788.2486920}. Additionally, users often do not posses the necessary terminology to precisely describe what they would like and what their problems are. Their results show the necessity for software support to categorize, analyze, and track user feedback \cite{Pagano:2013:UIS:2486788.2486920}.

Khalid~\textit{et~al.}~\cite{Khalid:2015:IEEE} ran a study over a set of over 6000 low-rated user reviews of 20 iOS apps. They discovered 12 types of user complaints, the most frequent of which were \textit{functional errors},  \textit{feature requests}, and \textit{app crashes}. The authors argue that developers should devote particular attention to user feedback, since negative reviews affect sales more than good review~\cite{Khalid:2015:IEEE}. By categorizing the user feedback, they believe developers can better plan their limited quality assurance (\textit{QA}) resources, by focusing on higher prioritized reviews. Their study results also stress the importance of establishing trust and meeting expectations with their user base~\cite{Khalid:2015:IEEE}.

\paragraph{Review Classification and Summarisation.}\label{review-classification-par}
As mentioned before, \textit{Review classification} is a subfield of \textit{App Store Mining} that concerns itself with automatically being able to classify reviews based on the textual features contained inside feedback.
In this regard, Chandy~and~Gu~\cite{chandy2012identifying} mined over six million reviews from the iOS App Store. After manually labeling a subset of the reviews as spam or not spam, they trained an unsupervised classifier to automatically categorize reviews taking into account average user ratings, and number of apps rated.
Continuing from here, Ha~\textit{et~al.}~\cite{6488439} manually examined reviews from the Google Play Store to determine whether users were talking about the privacy and security aspects of an app. From their sample, they determined that only 1\% of the user reviews mentioned these aspects.

Chen~\textit{et~al.}~\cite{Chen:2014:AMI:2568225.2568263} proposed \texttt{AR-MINER}, a framework for App Review Mining, capable of discerning informative reviews from the non-informative. Their approach works by (i) filtering out noisy and irrelevant reviews, (ii) using topic modelling to cluster reviews together by topic, (iii) ranking reviews, and (iv) presenting the ranked and clustered reviews to the developers.
They argue that their approach can facilitate the work of sifting through user feedback for big review sets.

Panichella~\textit{et~al.}~\cite{Panichella:2015:IIM:2881297.2881394} presented a system, capable of analyzing user reviews to support software maintenance and requirements evolution.
The authors presented a taxonomy to classify reviews, merging three techniques: (i) \textit{Natural Language Processing}, (ii) \textit{Text Analysis}, and (iii) \textit{Sentiment Analysis}. Later the authors further developed this idea and introduced \texttt{ARdoc}~\cite{panichella2016ardoc}, a review classifier, which this work heavily relies on. The authors divided reviews in 5 categories, presented in Table~\ref{ardoc-cat}. They argue that \texttt{ARdoc} could be useful in combination with topic modelling techniques, for example, dividing all reviews into categories before clustering, could provide a higher degree of cohesion inside the clusters.

\begin{table}[]
\centering
\caption{ARdoc categories~\cite{panichella2016ardoc}}
\label{ardoc-cat}
\begin{tabular}{|l|l|l|}
\hline
Category            & Description                                                                                                                                                & User Feedback Example                                                                                                         \\ \hline
Information Giving  & \begin{tabular}[c]{@{}l@{}}Sentences that inform or update \\ users or developers about an\\  aspect related to the app\end{tabular}                       & \begin{tabular}[c]{@{}l@{}}"This app runs so smoothly\\ and I rarely have issues\\ with it anymore"\end{tabular}              \\ \hline
Information Seeking & \begin{tabular}[c]{@{}l@{}}Sentences related to attempts to\\ obtain information or help from\\ other users or developers\end{tabular}                     & \begin{tabular}[c]{@{}l@{}}"Is there a way of getting\\ the last version back?"\end{tabular}                                  \\ \hline
Feature Request     & \begin{tabular}[c]{@{}l@{}}Sentences expressing ideas, \\ suggestions or needs for \\ improving or enhancing the app\\ or its functionalities\end{tabular} & \begin{tabular}[c]{@{}l@{}}"Please restore a way to\\ open links in external\\ browser or let us save\\ photos"\end{tabular} \\ \hline
Problem Discovery   & \begin{tabular}[c]{@{}l@{}}Sentences describing issues with\\ the app or unexpected behaviours\end{tabular}                                                & \begin{tabular}[c]{@{}l@{}}"App crashes when new power\\ up notice pops up"\end{tabular}                                      \\ \hline
Other               & \begin{tabular}[c]{@{}l@{}}Sentences do not providing any \\ useful feedback to developers\end{tabular}                                                    & "What a fun app"                                                                                                              \\ \hline
\end{tabular}
\end{table}

In recent work, Sorbo~\textit{et~al.} introduced \texttt{SURF} (Summarizer of User Reviews Feedback)~\cite{di2017surf}, a tool "able to (i) analyze and classify the information contained in app reviews and (ii) distill actionable change tasks for improving mobile applications"~\cite{di2017surf}. It is capable of summarizing thousands of user reviews to generate a task list of recommended software changes~\cite{di2017surf}.

\subsubsection{ChangeAdvisor}\label{sec:change-advisor-poc}
Finally, Palomba~\textit{et~al.} introduced \texttt{ChangeAdvisor}~\cite{Palomba2017}, a novel approach, built on the work of Panichella~\textit{et~al}~\cite{panichella2016ardoc}, able to cluster user reviews into topics, and link these topics to source code entities. Additionally they developed a \textit{Proof of Concept} (PoC) to demonstrate their approach.

The approach works as follows~\cite{Palomba2017}:
\begin{itemize}
\item User feedback is tagged based on predefined categories
\item Source code and feedback are preprocessed
\item Feedback belonging to the same category is clustered, to group together similar user needs
\item Determine code components related to user change requests
\end{itemize}

\paragraph{User Feedback Classification.}
In order to classify user feedback, \texttt{ChangeAdvisor} leverages \texttt{ARdoc}~\cite{panichella2016ardoc}.
Given user feedback, this classifier is capable of identifying the category the review belongs to. Table~\ref{ardoc-cat} shows an overview of the various categories.

\paragraph{Source Code and Feedback Preprocessing.}
In order to remove noise from both source code and feedback, the data set is pre-processed. The result of this step is a bag of words that will be used as input for the following steps.

\paragraph{User Feedback Clustering.}
Similar user needs are clustered in the following step. This is done for two reasons: (i) linking single reviews does not result in good accuracy results, and (ii) multiple users will probably have the same problems or needs that refer to the same change request. By clustering, this approach can achieve better results, all the while giving the developer a better overview of the users' perception and needs.

\paragraph{Recommending Source Code Changes.}
Clustered change requests (user feedback referring to the same task) are linked against the preprocessed source code component, to find out which set of classes need to be changed, according to user feedback. Links are computed using a similarity metric. Only pairs of code components and clusters that achieve a minimum similarity threshold are considered as related. The output is then a list of tuples $(cluster, component, similarity)$, \textit{links}, representing the reviews cluster, the source component, and their similarity metric. Concretely, each tuple is the class that needs to be changed in order to fulfil the users' request.

\paragraph{Limitations.}
The \texttt{ChangeAdvisor} tool at the moment exists solely as a PoC. It consists of a mix of Java code and Python Scripts, with some glue code in the form of Shell scripts inside a docker container. It takes the path to the source code and a review set and runs the process. As such, it is not yet flexible enough to be of real use for a team of developers, and is hard to maintain.
Ideally, in order to become productive with \texttt{ChangeAdvisor}, a developer would need to be able to configure it, according to its need. Such needs would include, at a minimum, the possibility to automatically import user reviews, according to a schedule, and the possibility to import source code, ideally from a \textit{Version Control System} (VCS). With these functions in place, the tools could be further developed to allow a developer to explore the reviews and the links to the source code by playing around with time intervals, running the linking only on certain categories, or even test different clustering methods or similarity metrics. 

Combining all these functions with a UI, would be of great help to developers, allowing them to gather more insight into their code, and explore the perception a user has of their work in many different ways.

This work, then, has as a goal, to use the \texttt{ChangeAdvisor}~\cite{Palomba2017} PoC, to lay the ground work for a new implementation of \texttt{ChangeAdvisor}, that is first of all configurable and maintainable. This tool should contain all of the features of the original work, plus new features such as a UI for the configuring of the parameters, scheduling of long running tasks, and a way to better explore user reviews through the use of visuals, such as diagrams. In doing so, it will become possible for future work to continue to evolve \texttt{ChangeAdvisor} adding new features, in order to better support developers, in the tedious task of gathering and understanding user change requests, by proposing entities that need change, in a fully automatic fashion. This would enable developers to leverage the wealth of information contained in reviews, and better plan their \textit{QA} resources.