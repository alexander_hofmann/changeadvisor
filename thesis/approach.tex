In this work, we aim to build a tool that is capable of a giving a team of developers insight into user reviews and the change requests contained within by (i) automatically importing user reviews, (ii) analyze these reviews in order to (iii) extract the top topics discussed and sentiments of the users, and finally (iv) link these topics to the source code which is going to need change. This tool is called \texttt{ChangeAdvisor} and it is composed of two parallel data processing pipelines. Figure~\ref{pipeline} shows the two pipelines; (i) the \textit{Review Pipeline} and (ii) \textit{the Code Pipeline}, and the key steps of each pipeline and how they come together to compute the \textit{links} between code and feedback.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.3]{imgs/update_ChangeAdvisorPipeline.pdf}
\caption{ChangeAdvisor Pipeline}
\label{pipeline}
\end{center}
\end{figure}

This Chapter explains the overall functioning of \texttt{ChangeAdvisor} as it was implemented for this thesis, while further implementation details can be found in Chapter~\ref{changeAdvisorMain}. Section~\ref{review-track} describes the \textit{Review Pipeline}, while Section~\ref{source-code-track} the \textit{Source Code Pipeline}. Section~\ref{linking-approach} explains how the linking algorithm determines links between source code and feedback. Finally, Section~\ref{sec:poc-and-changeadvisor}, gives a brief overview of the differences between the \textit{Proof of Concept} and the implementation done as part of this thesis.

\section{Review Pipeline}\label{review-track}
The \texttt{ChangeAdvisor} approach requires two inputs: user reviews and code. In this section, I shall describe the first pipeline, the \textit{Review Pipeline}, that contributes to the system by feeding the linking step with reviews.

User feedback come in many shapes and forms. If we consider feedback regarding a broken feature for example, the same problem might be described very differently by different people, ranging from the very generic "ur app is terrible, i cant even login!", to the more specific "I tried going under settings and tapping the blue button and then the app crashed. Happens on a Samsung Galaxy S7" and again to "Clicking on the \textit{log-in with Facebook} button doesn't work". All three reviews are discussing about the same problem they are having and yet are very different under many aspects. In order to be able to correctly identify similar feedback, certain steps are necessary to \textit{normalize} user reviews.

As such a pipeline for reviews was devised that could process reviews into a usable format for the \texttt{ChangeAdvisor} linker. Its main steps, which can be seen in Figure~\ref{review-pipeline}, are:

\begin{itemize}
\item the \textit{Review import} step imports users reviews into \texttt{ChangeAdvisor}. It uses the \textit{Review Crawler Tool}\cite{reviewCrawler} implemented in-house at UZH to mine user reviews from the \textit{Google Play Store}.

\item the \textit{Review Categorization and Analysis} step, processes user reviews into various categories such as \textit{FEATURE REQUEST} or \textit{BUG REPORT}, so that in the last step we can apply topic modelling more effectively, having grouped together reviews. In order to do so, we leverage \texttt{ARdoc}~\cite{panichella2016ardoc}, a review classifier which uses Natural Language Processing (NLP), Sentiment Analysis and Text Analysis to classify reviews.

\item the \textit{Preprocessing }step handles the \textit{normalization} of user feedback, in order to reduce noise in the reviews. This step is composed of classic NLP techniques such as \textit{stop word} removal, \textit{stemming}, and \textit{Part-of-Speech filtering} in order to clean up user feedback.

\item the \textit{Clustering} step groups reviews discussing the same change request together. Two clustering algorithms were implemented for this work: \textbf{Hierarchical Dirichlet Process}~\cite{teh2005sharing}~(HDP, \ref{hdp}) and \textbf{Term Frequency-inverse Document Frequency}~\cite{doi:10.1108/eb026526} (TFIDF, \ref{tfidf}).
\end{itemize}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.35]{imgs/update_Review_Pipeline.pdf}
\caption{Review pipeline. The result of each step, represents the input to the following one.}
\label{review-pipeline}
\end{center}
\end{figure}

\subsection{Review Import}
As mentioned, \texttt{ChangeAdvisor} requires reviews as an input. As such, this work includes the functionality to mine user reviews from the \textit{Google Play Store}. 
This import function takes the \textit{Google Play Store Id} of an app, and includes the possibility to import reviews either in a scheduled manner or triggered by user action.
The review miner was written at the University of Zurich~\cite{reviewCrawler} and was integrated into \texttt{ChangeAdvisor}. This functionality represents one of the biggest changes in respect to the PoC. With the proof of concept, a developer would have had to separately export all his reviews, maybe writing them to a database or to a file. \texttt{ChangeAdvisor} would then run its computation over the entire data set.
If he wanted to regularly use \texttt{ChangeAdvisor}, he would have to regularly append the most recent reviews export to his database, export into a format suitable for the tool, and then re-run the same computation even though only a small part of the ever-growing data set has changed.
Thus, providing the possibility for a user of \texttt{ChangeAdvisor} to import reviews directly into the tool, using a predefined schedule, or even just by manually triggering the process, represents a huge step forward in terms of usability.

\subsection{Review Analysis}
The goal of the \textit{Review Analysis} step is to categorize reviews, as a pre-clustering step, to increase the overall accuracy of the system, as mentioned in Section~\ref{review-classification-par}. Reviews present in the database are analyzed using \texttt{ARdoc}~\cite{panichella2016ardoc}, in order to assign to the feedback one of the categories presented in~\cite{Panichella:2015:IIM:2881297.2881394}. An overview of these categories can be seen in Table~\ref{ardoc-cat}.
A review might be composed of multiple sentences, while \texttt{ARdoc} analyzes single sentences in isolation.
Consider a review such as "Great game very addicting would give it 5 stars. Unfortunately ever since the last update, the game crashes after the fourth turn". This review is composed of two sentences, the first part would be categorized as \textit{Information Giving}, while the second part as \textit{Problem Discovery}.
 This example shows that a single review might have different categories assigned to each sentence of the review. To keep track of context after processing, the review is split into it's composing sentences and saved separately, but each with a reference to the original sentence. Thus, the example above, would become two separate entities in the review pipeline. This is particularly useful when showing the linking results at the end, where the context of the entire sentence might be of use, instead of just part of it.

\subsection{Review Preprocessing}\label{review-preprocessing}
Preprocessing is necessary in order to remove the noise contained in user reviews that might hinder the accuracy of the NLP techniques used and to make reviews consistent to each other. For this a series of steps were defined to transform the categorized reviews into input suitable for \texttt{ChangeAdvisor}:

\paragraph{Sentence Correction.} The feedback's sentences are checked for spelling and grammar mistakes using the \textbf{LanguageTool}~\cite{languageTool}~API. This tool is capable of parsing sentences and suggesting changes to correct them, but it is only able to make suggestions. Because of this, \texttt{ChangeAdvisor} applies each suggestion automatically. Since \textbf{LanguageTool} checks both spelling and grammar, it comes with a noticeable performance penalty, when processing thousands of reviews. Future work might want to look into possible alternatives for sentence correction, or limiting the correction to only spelling mistakes.

\paragraph{Contractions Expansion.} All English (colloquial) contractions are replaced with their expanded forms (e.g. "it's" becomes "it is"). Contractions are identified by the use of regular expressions for both two parts contractions (e.g. "it's") and three parts contractions (e.g. "wouldn't've"). Once a contraction is identified, its expanded form is found from a dictionary of contractions and it is replaced.

\paragraph{Tokenization and Part of Speech Tagging (PoS).}
User feedback is split into tokens using the well known \textbf{Stanford CoreNLP} library~\cite{manning-EtAl:2014:P14-5}. Tokenization follows the rules of the Penn Treebank tokenization~\cite{Marcus93buildinga} using Stanford's \texttt{PTBTokenizer} and works by enhancing each parsed token with its PoS tag and lemma, which will be useful in the following two steps \textit{Nouns and Verbs filtering} and \textit{Singularization}. Depending on the clustering algorithm used in the following steps of the pipeline, token order may or may not be maintained.

\paragraph{Nouns and Verbs filtering.} According to Capobianco~\textit{et~al.}~\cite{capobianco2013improving}, nouns and verbs carry the most meaning inside a document and can greatly help in increasing the accuracy of Information-Retrieval (IR) tasks. As such, in the \textit{Preprocessing} step, we filter our document to keep only verbs and nouns. Nouns and verbs are identified in the previous step were each token was tagged with its PoS tag by the tokenizer.

\paragraph{Singularization.} Tokens are normalized by means of transforming all plural forms into their singular forms. This is done by using their lemma.

\paragraph{Stop word removal.} Stop words are all those words that carry little to no semantic meaning (e.g. "the", "a", etc...). Since these words carry no real meaning, they are discarded at this step. A stop word dictionary and a filter is included together with \texttt{ChangeAdvisor}. Future work may want to extend this dictionary or add support for multiple languages by extending the filter.

\paragraph{Stemming.} Each token is reduced to its stem utilizing Porter's stemmer~\cite{porter1980algorithm}, to reduce the variation, since tokens such as "play", "playing", and "played" all convey the same semantic.

\paragraph{Repetition removal.} As mentioned above, depending on the clustering algorithm used next, duplicates may or may not be allowed. As an example, \textbf{TF-IDF}~\cite{doi:10.1108/eb026526} clustering on N-gram tokens would lose it's significance if duplicates were removed, since it would greatly change the structure of a sentence.

\paragraph{Short tokens removal.} As with \textit{repetition removal}, depending on the clustering used, short tokens may or may not be removed. According to Mahmoud~\textit{et~al.}~\cite{mahmoud2015role}, tokens containing less than 3 characters are usually irrelevant for IR purposes.

\paragraph{Short documents removal.} In this step, document shorter than three tokens are discarded, since they cannot convey a change request clearly, according to Palomba~\textit{et~al.}~\cite{Palomba2017}.

\paragraph{Preprocessing Example.}
Below we can find an example of running our IR preprocessing on a sample review:

\noindent\fbox{
\parbox{\textwidth}{
\textit{"One thing that I would really love if this app had is if it lets you create an account (or log in with your email) because whenever I get a new phone, or my phone's been reseted, I need to download the app and music all over again, which can waste a bit of time (especially since I've got lots of music on this app)."}}}

\noindent We now process the review, maintaining duplicates and order, but removing stop words and using a PoS-filter:

\noindent\fbox{
\parbox{\textwidth}{
\textit{"\textcolor{gray}{One thing that I would really \textcolor{black}{love} if this \textcolor{black}{app} had is if it lets you \textcolor{black}{creat}e an \textcolor{black}{account} (or \textcolor{black}{log} in with your \textcolor{black}{email}) because whenever I get a new \textcolor{black}{phone}, or my \textcolor{black}{phone}'s been \textcolor{black}{reset}ed, I need to \textcolor{black}{download} the \textcolor{black}{app} and \textcolor{black}{music} all over again, which can \textcolor{black}{wast}e a \textcolor{black}{bit} of \textcolor{black}{time} (especially since I've got \textcolor{black}{lot}s of \textcolor{black}{music} on this \textcolor{black}{app}).}"}}}

\noindent We then stem each token:

\noindent\fbox{
\parbox{\textwidth}{
\textit{["love", "app", "creat\textcolor{gray}{e}", "account", "log", "email", "phone", "phone", "reset\textcolor{gray}{ed}", "download", "app", "music", "wast\textcolor{gray}{e}", "bit", "time", "lot\textcolor{gray}{s}", "music", "app"]}}}

\noindent And the resulting Bag-of-Words after the \textit{Preprocessing} step is:


\noindent\fbox{
\parbox{\textwidth}{
\textit{["love", "app", "creat", "account", "log", "email", "phone", "phone", "reset", "download", "app", "music", "wast", "bit", "time", "lot", "music", "app"]}}}

At the end of this step, we have a set of bag-of-words, that will be suitable to be used as input for the clustering and linking steps.

\subsection{Clustering}\label{sec:clustering}
Clustering is the process of grouping together a set of objects, which in some way, are more similar to one another, than to those belonging to other groups. Each group is then called a \textit{cluster}.
This process works by defining a set of \textit{features} we use to compare one item to another and a metric, in order to quantitatively measure the similarity, in terms of distance between the features of any two items.

Consider the following example, adapted from the \texttt{ChangeAdvisor} paper~\cite{Palomba2017}:
\textit{John} is the mobile app developer behind, \texttt{FrostWire}, a BitTorrent client for Android, with a presence on Google Play Store. \texttt{FrostWire} has quite a user base at the moment. In an attempt to increase his user base even further, \textit{John} recently released an overhaul of the app, redesigning the UI and adding new features. Suddenly, the average ratings for his app are plummeting, and many users are complaining in their reviews. A lot of people seem to be complaining about having problems downloading files, while others seem to be having various usability difficulties. From the sheer amount of reviews alone, it is hard to judge, exactly, how many unique problems there are. 
So, \textit{John} starts sifting through reviews, trying to isolate the various problems his users are having.
Unfortunately, there are too many reviews to go through, so after a while, \textit{John} stops and starts to plan, which issue he wants to tackle first.
This is where clustering comes into play. By running a clustering algorithm, we can do this job automatically, without having \textit{John} go through the trouble of reading all the reviews. Now, he has an overview of how many problems the current version of the app has, and can plan each fix and their priority based on gravity and number of people discussing this problem.



The goal of the \textit{Clustering} step is, thus, to group together feedback discussing the same change requests. This step is required and is useful for two reasons: (i) as has been shown in the work of Palomba~\textit{et~al.}~\cite{Palomba2017}, linking single reviews tends to return poor results, and (ii) clustering allows us to group together feedback referring ideally to the same, or at the very least similar, change request, thus avoiding duplicates and making the system more comprehensible and generally easier to follow for the developer.

The original \texttt{ChangeAdvisor} paper experimented with three different methods for clustering textual documents: (i) \textbf{Latent Dirichlet Allocation} (LDA)~\cite{asuncion2010software}, (ii) \textbf{genetic algorithms applied to LDA} (LDA-GA)~\cite{panichella2013effectively}, and (iii) \textbf{Hierarchical Dirichlet Process} (HDP)~\cite{teh2005sharing}.
While experimenting, they found that \textbf{LDA-GA} was not efficient enough, and thus running it against a big data set (in the order of thousands of reviews) wasn't feasible.
The original \texttt{ChangeAdvisor} PoC chose to implement \textbf{HDP}, as the authors found that it provided a good balance between speed of execution and quality of results. Thus for this work, \textbf{HDP} was implemented as well~(\ref{hdp}).
Additionally this work experimented with \textbf{Term Frequency-inverse Document Frequency} (TF-IDF) as well, the details of which can be found in Section~\ref{tfidf}

\subsubsection{Hierarchical Dirichlet Process}\label{hdp}
\textbf{Hierarchical Dirichlet Process}~(HDP)~\cite{teh2005sharing}, is a non-parametric Bayesian model for topic extraction. It is an extension of \textbf{Latent Dirichlet Allocation}~(LDA), with the advantage that it does not require the number of topics to be known a priori. Given that we have thousand of reviews and we attempt to cluster reviews by change requests, there is no way to determine the number of clusters in advance. \textbf{HDP} trains a hierarchical model to distinguish the topics, i.e. clusters, over a fixed number of iterations, based on a probability distribution, the \textbf{Dirichlet Process}~\cite{teh2005sharing}.

Ideally at the end of this clustering step, each resulting cluster, i.e. topic, should contain a set of reviews and a Bag-of-words containing the features extracted from these reviews. Each cluster should identify a unique change requests.
The Bag-of-words are the most meaningful words representing a cluster and are then used as a sort of label while presenting the results to the user.

The following shows an example of a topic and two reviews that were assigned to it:

\begin{itemize}
\item Topic: One of the topics (in Bag-of-Words format) that was found using \textbf{HDP} on a set of reviews for \texttt{FrostWire}: \\ \noindent\fbox{
\parbox{\textwidth}{
\textit{[ 
        "app", 
        "love", 
        "reinstal", 
        "error", 
        "look", 
        "version", 
        "lot", 
        "download", 
        "file", 
        "fix", 
        "phone", 
        "try", 
        "gui", 
        "sai", 
        "time", 
        "wast", 
        "issu", 
        "internet", 
        "card"
    ]}}}

\item Two of the reviews that were assigned to this topic: 
\begin{enumerate}
\item \noindent\fbox{
\parbox{\textwidth}{
\textit{"I try to get the plus version and every time I open the file it keeps saying can't open file"
}}}

\item \noindent\fbox{
\parbox{\textwidth}{
\textit{"[...] Also when I downloaded some songs and days later I go back to play them,\\ the app would display a message saying "file not available" when it clearly shows on my playlist.
}}}
\end{enumerate}

\item And their Bag-of-Words, which are used to link a review to a topic:
\begin{enumerate}
\item \noindent\fbox{
\parbox{\textwidth}{
\textit{[
    "file",
    "sai",
    "try",
    "time",
    "version"
]
}}}

\item \noindent\fbox{
\parbox{\textwidth}{
\textit{[
    "song",
    "app",
    "plai",
    "download",
    "file",
    "playlist",
    "displai",
    "sai",
    "messag",
    "dai"
]
}}}
\end{enumerate}

\end{itemize}


\subsubsection{TF-IDF}\label{tfidf}
As can be seen above, \textbf{HDP} computes topics, where each topic represents a change request. A topic is essentially composed of a list of tokens, representative of all reviews discussed inside said topic. This list of tokens is then used as a sort of label, to quickly give an idea to the developer about what a set of reviews is talking about.
However, as in the example above, a label consisting of 20 or more tokens doesn't convey the information about the topic very well. Because of this, a second approach was experimented for the computation of clusters: \textbf{Term~Frequency-inverse~Document~Frequency}~(TF-IDF)~\cite{doi:10.1108/eb026526}. \textbf{TF-IDF} is one of the most popular term-weighting scheme and is a technique often used in Information-Retrieval and text mining. It is commonly used on many websites, to automatically tag posts and articles, by determining the most relevant terms in a document. It works by weighting the frequency of a token inside a document against the number of documents in which the token appears over the entire corpus.

More specifically the \textbf{TF-IDF} is the product of \textit{Term Frequency} (TF) and \textit{Inverse Document Frequency}~\cite{doi:10.1108/eb026526} (IDF). While there are multiple different formulations of \textbf{TF} and \textbf{IDF}, the following shows the way it was applied for \texttt{ChangeAdvisor}.

\paragraph{Term Frequency.}
The \textit{Term Frequency} $tf(t, d)$ is the frequency of a token $t$ inside a document $d$. Simply put, it is the number of occurrences of $t$ in the document $d$, $n_{t,d}$, divided by the number of tokens in $d$, $|d|$~\cite{manning2008introIR}:
$$
tf(t, d) = \frac{n_{t,d}}{| d |}
$$

A high $tf(t,d)$ means that the token $t$ appears many times in respect to the entirety of the document, which might indicate higher relevance. However, \textbf{TF} considers all tokens equally as relevant, which is not always the case. Consider for example this thesis: the words \textit{"review"} and \textit{"feedback"} appear many times. The resulting \textbf{TF} would probably be higher than any other word (aside from stop words). However, the above mentioned words probably convey little meaning in the context of this work. So in order to compute the actual relevancy of the token, $tf(t,d)$ is weighted by multiplying it with the \textit{Inverse Document Frequency}.

\paragraph{Inverse Document Frequency.}
The \textit{Inverse Document Frequency} (IDF) is used to attenuate the effect of terms, that appear too often to be relevant in the context of the entire corpus. For this we use the \textit{inverse document frequency} $idf(t, C)$~\cite{manning2008introIR}:
$$
idf(t, C) = \frac{N}{| d \in C : t \in d |}
$$
where $N$ is the number of documents inside the corpus $C$ and $| d \in C : t \in d |$ is the number of document in which the token $t$ occurs. Thus, a token that occurs in many documents scores lower than one that appears in many documents.

\paragraph{TF-IDF.} The \textit{Term Frequency-inverse Document Frequency} metric is simply the product of \textbf{TF} and \textbf{IDF}, or in other words, the \textit{TF-IDF} score of a token $t$ in a document $d$ belonging to a corpus $C$ is~\cite{manning2008introIR}:
$$
tf\text{-}idf(t, d, C) = tf(t, d) \times idf(t, C)
$$
where a higher \textit{tf-idf} weight for a token $t$ implies a higher relevancy inside a document $d$, whereas a lower weight implies that $t$ is not as relevant in the context of the document $d$.

\paragraph{TFIDF and ChangeAdvisor}
Given that \textit{TF-IDF} computes the relevancy of a token inside a document, the choice of \textit{what is} a document becomes particularly important, i.e. a single review should not be chosen as a document for certain reasons. The problem is that the $tf$ term of \textit{TF-IDF} is almost a binary value in the context of reviews and as such it does not do a good job as a discriminating feature. This is caused by the fact that reviews typically are very short and rarely contain duplicate terms (exception made for stop words). The same problem was mentioned by Naveed~\textit{et~al.}~\cite{naveed2011searching}. The authors researched new methods to extract features from Twitter posts. Twitter posts because of their nature, are similar to user feedback in that it is kept short (< 140 characters) and informal.
Additionally we do not care about the importance of a token inside a review but rather the relevance of a token inside the set of reviews labeled with a given \texttt{ARdoc} category.
Because of this, the set of all reviews belonging to an \texttt{ARdoc} category was chosen as a document $d$ while the corpus $C$ is composed of the set of all reviews for a particular app.
Additionally the \textbf{TF-IDF} component developed for \texttt{ChangeAdvisor} allows the possibility to compute the score using single \textbf{tokens} as term $t$, as well as \textbf{N-grams} (further details in Section~\ref{changeAdvisorMain}.

The process of clustering using \textit{TF-IDF} then works as follows and can be seen in Figure~\ref{tf-idf}.
\begin{itemize}
\item For each \texttt{ARdoc} category fetch all reviews belonging to said category, this is our document $d$.
\item For each token $t \in d$, compute the \textit{TF-IDF} weight $tf$-$idf(t, d, C)$.
\item For each document $d$, fetch the $N$ tokens with the highest \textit{TF-IDF} weight.
\item For each of the fetched tokens $t_i$, fetch all reviews in the same \texttt{ARdoc} category that contain said token $t_i$.
\end{itemize}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.35]{imgs/update_TF-IDF.pdf}
\caption{TF-IDF clustering approach in \texttt{ChangeAdvisor}. 1) The initial set of reviews. 2) The reviews are grouped by \texttt{ARdoc} category. 3) We run TF-IDF for each group. 4) For each group we take the top N tokens with the highest TF-IDF score and fetch the reviews containing said token.}
\label{tf-idf}
\end{center}
\end{figure}

\newpage
The following shows an example of a cluster determined by using \textbf{TF-IDF}, for the category \textit{"Problem Discovery"}, with an N-gram size of 1, for the app \texttt{FrostWire}:

\begin{itemize}
\item Topic: One of the topics found using \textbf{TF-IDF}
\\ \noindent\fbox{
\parbox{0.8\textwidth}{
\textit{"Crash"}}}

\item Three reviews, verbatim, that were assigned to this topic and belong to the category \textit{"Problem Discovery"}:
\begin{enumerate}
\item \noindent\fbox{
\parbox{0.8\textwidth}{
\textit{"I updated it when the description said the crashes we're fixed. \\ Literally after I updated and started playing my music it crashed twice. Please fix it.."}}}

\item \noindent\fbox{
\parbox{0.8\textwidth}{
\textit{"It crashes and I lose all of my torrent and downloads stop. Edit: now it turns itself on. Grrrr"}}}

\item \noindent\fbox{
\parbox{0.8\textwidth}{
\textit{"It was okay at first but then it kept crashing and couldn't download or find hardly any rock bands I looked up. If they fixed that it would be great."}}}
\end{enumerate}
\end{itemize}

\section{Source Code Pipeline}\label{source-code-track}
The second input to the \texttt{ChangeAdvisor} process is source code. Similarly as with reviews, source code is also plagued by noise, which is why a second pipeline was devised specifically for it. This second pipeline, is much simpler than the first one. This is because source code doesn't have the same nature of reviews: (i) documents do not tend to be short, (ii) they adhere, syntactically, to the strict rules of a programming language, and (iii) even in teams of developers, guidelines are usually enforced helping making the source code more homogeneous. These reasons contribute to a lower overall noise level than online reviews. Thus, this simpler pipeline, which can be seen in Figure~\ref{fig:source-pipeline}, is divided in two steps:

\begin{itemize}
\item Source Code Import: source code is imported from the file system or from version control.
\item Source Code Processing: source code is processed into a suitable format for input into the \texttt{ChangeAdvisor} linker.
\end{itemize}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.35]{imgs/update_source-pipeline.pdf}
\caption{Source Code pipeline. Again, the results of a step, are passed to the following one as inputs.}
\label{fig:source-pipeline}
\end{center}
\end{figure}


\subsection{Source Code Import}\label{sec:source-code-import}
The goal of this step is to import the source code into \texttt{ChangeAdvisor}. Source code that is imported, is \textit{parsed} and divided into its components. We define a \textit{source code element} as the basic building block of an application, i.e. in Java a \textit{class}. It is important to note that we use Java \textit{classes} as \textit{elements} and not compilation units (i.e. .java files) which might be composed of multiple classes, e.g. nested classes. Furthermore, we exclude \textit{interfaces} from being \textit{elements} as they do not contain any implemented logic and as such would rarely be targets of change requests. Although Java 8 introduced default methods which add implementation to an interface, this is not used as often. Future work might want to look into this, especially considering the recent release of Java 9 and the increased adoption of Java 8. As such, candidates for being picked up as \textit{source code element} are classes (normal, nested, and static nested) and enums.

As in the PoC, for each \textit{source code element}, we parse its public API and related comments. The terms of each \textit{element} are collected for processing in the following step.

Here, again, the biggest newest feature is the possibility to automatically import source code from \textit{git}, which increases the overall usability of the tool. Other VCS options, can easily be added in future. Further details can be found in Chapter~\ref{changeAdvisorMain}.

\subsection{Source Code Preprocessing}\label{sec:code-preprocessing}
In the preprocessing step, we normalize the terms of each \textit{source code element} to lower the noise level and to bring the \textit{elements} into a usable format for linking.
The preprocessing steps are similar to those of the \textit{Review pipeline} seen in Section~\ref{review-preprocessing}:

\paragraph{Separation of composed identifiers.} We split composed identifiers, commonly found in many programming languages, such as camelCase, snake\_case and digit separated text. To do this, we employ regular expressions.

\paragraph{Removal of special characters.} Special characters such as brackets are removed from the document as they do not convey any semantics.

\paragraph{Removal of stop words.} In addition to normal english stop words, source code also includes programming stop words, i.e. terms such as \textit{private}, \textit{public}, \textit{void}, etc.
These terms are also removed as they are not usable as discerning features.

\paragraph{Stemming.} We normalize each token by replacing it with its stem, computed using Porter's stemmer~\cite{porter1980algorithm}.

\paragraph{Removal of short tokens.} As a last step, we remove all tokens shorter than a threshold, for the same reason we removed them during the \textit{Review Preprocessing} step~(\ref{review-preprocessing}).

At the end of this step, we should, ideally, have a set of Bag-of-words, each representing a \textit{source code element}, suitable to be used for linking with reviews.

\section{Linking}\label{linking-approach}
Regardless of the clustering algorithm used for reviews, we now have a set of clusters, each containing an \texttt{ARdoc} category~\cite{panichella2016ardoc} and a set of reviews as Bag-of-Words, ideally representing the same, or at least similar, change requests, and a set of \textit{source code elements}, also in Bag-of-words format.

Our goal is to find links between our feedback clusters and source code, each link representing a set of classes that are probable candidates for modification, in order to fulfill the change request represented by the cluster. In order to compute links, we must first define how we measure the \textit{vicinity} of two documents, i.e. a \textit{metric}.

\subsection{Metric}
As mentioned, we must first define a \textit{metric}. As it was the case in the \texttt{ChangeAdvisor} PoC, we compute the similarity using the Asymmetric Dice coefficient, the definition of which can be found below~\cite{Palomba2017}:

$$
\textrm{similarity}(cluster_i, source_j) = \frac{| W_{cluster_i} \cap W_{source_j} |}{min(| W_{cluster_i} |, | W_{source_j} | )}
$$

where $W_{cluster_i}$ represents the set of words contained in cluster $i$. $W_{source_j}$ represents the set of words contained in the \textit{source code element} $j$. The $min$ function normalizes the similarity score with respect to the shortest document between $i$ and $j$. We normalize with respect to the shortest document, because user feedback is, in most cases, considerably shorter than our \textit{source code elements}.
Due to the normalization step, the Dice coefficient range is $[0, 1]$.

\subsection{ChangeAdvisor Linking}\label{sec:changeadvisor-linking}
Having defined the inputs, and a suitable metric for document similarity, we now define the steps and conditions needed to compute links between a \textit{review cluster} and a \textit{source code element}.
Given a set $E_{source}$ which represents the set of all \textit{source code elements}, $C_{cluster}$ the set of all clusters, and $T$ the threshold to reach in order for a cluster and element to be considered a link:

\begin{itemize}
\item define a bucket $b_i$ for each cluster $c_i \in C_{clusters}$.
\item for each cluster $c_i$ compute the Dice coefficient score with each code element $e_j \in E_{source}$.
\item if the Dice score reaches the threshold $T$, add the \textit{element} $e_j$ to the bucket $b_i$
\end{itemize}

The linking process can be seen in Figure~\ref{fig:linking}. At the end of these simple steps, we shall have a set of buckets, where each bucket $b_i$ contains the set of all links found for the cluster $c_i$, or an empty set, meaning that no links were found for a given change request.


\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.5]{imgs/linking_clustering.pdf}
\caption{\texttt{ChangeAdvisor} linking.}
\label{fig:linking}
\end{center}
\end{figure}

\section{ChangeAdvisor Proof of Concept Limitations}\label{sec:poc-and-changeadvisor}
This chapter gave an overview of the approach implemented in \texttt{ChangeAdvisor}. This is mostly unaltered from the approach applied in the Proof of Concept~\cite{ChangeAdvisor2017PoC}. During the development of this thesis, it became apparent that the PoC deviates slightly from the process as explained in the work of Palomba~\textit{et~al.}~\cite{Palomba2017}. Thus for the implementation, we chose to mostly concentrate on the PoC interpretation of \texttt{ChangeAdvisor}. 


However, the PoC is limited under many aspects. Under the usability aspect, the PoC is run as a command line program. It outputs its results in a CSV (\textit{comma separated values}) file. It is not possible, to visually explore the results using charts and diagrams for example, which could help in understanding and interpreting the results.
Additionally a user is required to dump his reviews in a plain text file. Source code is imported by providing the path to the root folder of an app. The PoC doesn't persist any values between runs so, every time we want to re-run \texttt{ChangeAdvisor} we need to create an updated version of the review file, which in turn, greatly impacts its efficiency: the PoC will then recompute all values from scratch, which means that as the code base grows and, in particular, the number of reviews increases, \texttt{ChangeAdvisor} will become more and more slow.
There is also no support for stopping and restarting a job at a later time. Once the PoC has started, it either has to go through the entire process, which might take hours for a relatively small data set, or if we want to interrupt it, we will have to restart the process from the beginning at a later time.

The PoC is also not configurable in any way. All of its steps are hard-coded, so it isn't possible to, say, swap out a clustering algorithm for another one, or modify the preprocessing steps,  at run-time. This kind of configurability would require a substantial change in the tool's design and a complete rewrite of the source code.

On the maintainability side of things, it is very hard to maintain a code base, consisting of various scripts written in different languages and brought together by glue code. A maintainer of the tool might find himself tracking down a bug through different applications.


Thus, one of the main contributions of this work, is the complete rewrite of \texttt{ChangeAdvisor} as a cohesive Java application, including a GUI in the form of a web application. 
This all translates to:
\begin{itemize}
\item Increased usability for the developer: features such as the automatic import of reviews based on a schedule, import of code through VCS, visual representation of reviews, ratings distributions in the form of diagrams and charts, all greatly help in making the best use of a developer's time when dealing with feedback.
Not to mention, that the GUI greatly simplifies the use of \texttt{ChangeAdvisor} under many  key aspects: 
\begin{itemize}
\item Configuration: e.g. creation of a project, setting a schedule, setting the remote path to a repository, etc.
\item Exploration of results: e.g. the usage of labels computed through \textbf{TFIDF} clustering, for example, can more quickly transmit to the developer, what the main topics discussed in the reviews are.
\item Starting long-running jobs: e.g. even with a schedule, it is still possible to manually start a review import.
\end{itemize}

\item Increased performance: we leverage the database, in order to not have to recompute values that can be persisted. Given, that the database contains data of a previous pipeline step, a job can resume from the following step. Additionally, we can use the database for costly operations such as search and aggregation operations.

\item Increased configurability for the researcher, testing possible research questions and links between reviews and code (e.g. \textit{Release Engineering}), allowing them to run \texttt{ChangeAdvisor} with different processing steps and/or clustering algorithms.

\item Increased maintainability and extensibility, since \texttt{ChangeAdvisor} itself is an application, which undergoes evolution, in much the same way, as the apps it analyzes.
\end{itemize}

The concrete details of all changes can be found in Chapter~\ref{changeAdvisorMain}.