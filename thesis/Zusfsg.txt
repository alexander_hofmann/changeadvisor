Zusammenfassung
Das Benutzerfeedback spielt eine wichtige Rolle bei der Entwicklung und Wartung von Mobile
Apps. Die Erfahrung, die ein Endbenutzer mit einer App hat, ist einer der wichtigsten Punkte
bei der Entwicklung und Wartung eines erfolgreichen Produkts. Aus diesem Grund müssen Entwicklerteams Meinungen und Feedbacks von End-Usern in den Entwicklungsprozess ihrer Software einfliessen lassen, um den Marktanforderungen gerecht zu werden. Bestehende App Stores
bieten Entwicklern jedoch nur begrenzte Unterstützung, um Benutzerfeedbacks systematisch zu
filtern, zu aggregieren, und zu klassifizieren. Ebenso fehlt es an Techniken, um aus diesen Feedbacks Anforderungen an das Produkt herzuleiten. Ausserdem ist das Lesen jener Feedbacks nicht
praktikabel, wenn man die Menge an täglichen Bewertungen für beliebte Apps berücksichtigt.
Selbst wenn die Menge an Bewertungen kein Problem wäre, sind die gesammelten Informationen auf Benutzerbewertungen beschränkt und es gibt keine Möglichkeit die Benutzerfeedbacks
mit den zu ändernden Quellcode-Komponenten systematisch zu verlinken. Dies wäre nur mit
enormen manuellem Aufwand zu ermöglichen und wäre sehr fehleranfällig.
Um diese Lücke zu füllen, führte Palomba et al. [PSC+ 18] ChangeAdvisor ein. Der ChangeAdvisor Ansatz erlaubt es, Benutzerfeedbacks, welche für die Softwarewartung nützlich sind, nach
Themen zu gruppieren. Dank diesen Themengruppen sehen die Entwickler welche QuellcodeKompenenten verbessert werden sollten. Dadurch wird die Arbeit der Entwickler bereits vereinfacht, da es nicht mehr notwendig ist die Reviews manuell zu durchsuchen, zu unterteilen und
den Quellcode-Komponenten zuzuordnen.
Bisher war ChangeAdvisor nur ein Proof of Concept, welcher in Bezug auf Wartbarkeit, Erweiterbarkeit und Funktionalität sehr eingeschränkt war. In dieser Bachelorarbeit wird ChangeAdvisor deshalb als Softwarebibliothek implementiert, um zukünftige Erweiterungen zu unterstützen.
Zusätzlich wurde eine Client-Server App implementiert, welche es Entwicklern erlaubt die gesamten
Informationen aus Benutzerfeedbacks zu nutzen.