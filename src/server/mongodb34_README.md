Installing mongodb 3.4 in macOS:

```
brew install mongodb@3.4
echo 'export PATH="/usr/local/opt/mongodb@3.4/bin:$PATH"' >>  ~/.bash_profile
source ~/.bash_profile
```
